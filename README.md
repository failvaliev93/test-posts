# Test React проект на FSD

[Ссылка проекта на GitLab](https://gitlab.com/failvaliev93/test-posts.git).

Технологии: 
- React
- [React-bootstrap](https://react-bootstrap.netlify.app)
- React-router
- Axios
- Redux
- [Feature-Sliced Design](https://feature-sliced.design)

