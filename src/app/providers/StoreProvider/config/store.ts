import {
  CombinedState,
  configureStore,
  Reducer,
  ReducersMapObject,
} from '@reduxjs/toolkit';
import { api } from 'shared/api/api';
import { StateSchema, ThunkExtraArg } from './StateSchema';
import { createReducerManager } from './reducerManager';
import { profileReducer } from 'features/profileCard/model/slice/profileSlice';
import { userReducer } from 'entities/User/model/slice/userSlice';
import { userPostsReducer } from 'features/userPosts/model/slice/userPostsSlice';
import { postsReducer } from 'features/posts/model/slice/postsSlice';

export function createReduxStore(
  initState?: StateSchema,
) {
  const rootReducers: ReducersMapObject<StateSchema> = {
    user: userReducer,
    profile: profileReducer,
    userPosts: userPostsReducer,
    posts: postsReducer,
  };

  const reducerManager = createReducerManager(rootReducers);

  const extraArg: ThunkExtraArg = {
    api,
  };

  const store = configureStore({
    reducer: reducerManager.reduce as Reducer<CombinedState<StateSchema>>,
    preloadedState: initState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        thunk: {
          extraArgument: extraArg,
        },
      }),
  });

  // @ts-ignore
  store.reducerManager = reducerManager;

  return store;
}

export type AppDispatch = ReturnType<typeof createReduxStore>['dispatch'];
