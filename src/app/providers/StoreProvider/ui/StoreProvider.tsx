import { PropsWithChildren } from 'react';
import { Provider } from 'react-redux';
import { StateSchema } from '../config/StateSchema';
import { createReduxStore } from '../config/store';

interface StoreProviderProps {
  initState?: DeepPartial<StateSchema>;
}

export const StoreProvider = (props: PropsWithChildren<StoreProviderProps>) => {
  const { children, initState } = props;

  const store = createReduxStore(
    initState as StateSchema,
  );

  return <Provider store={store}>{children}</Provider>;
};
