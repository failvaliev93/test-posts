import React, { useEffect } from 'react';
import { AppRouter } from './providers/router';
import { Navbar } from 'widgets/Navbar';
import { useAppDispatch } from 'shared/lib/hooks/useAppDispatch/useAppDispatch';
import { initAuthData } from 'entities/User';

export const App = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(initAuthData());
  }, [dispatch]);

  return (
    <div className="App">
      <Navbar />
      <AppRouter />
    </div>
  );
}
