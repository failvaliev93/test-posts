import { classNames } from 'shared/lib/classNames/classNames';
import { Comment } from '../../model/types/comment';
import { Card } from 'react-bootstrap';

interface CommentListItemProps {
  comment: Comment;
  className?: string;
}

export const CommentListItem = (props: CommentListItemProps) => {
  const { className, comment } = props;

  return (
    <Card className={classNames('', {}, [className])}>
      <Card.Body>
        <p>{comment.email} пишет:</p>
        <Card.Text>{comment.body}</Card.Text>
      </Card.Body>
    </Card>
  );
};