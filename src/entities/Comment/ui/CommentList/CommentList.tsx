import { classNames } from 'shared/lib/classNames/classNames';
import s from './CommentList.module.scss';
import { Comment } from '../../model/types/comment';
import { Stack } from 'react-bootstrap';
import { Loader } from 'shared/ui/Loader';
import { CommentListItem } from '../CommentListItem/CommentListItem';

interface CommentListProps {
  comments?: Comment[];
  className?: string;
  isLoading?: boolean;
  error?: string;
}

export const CommentList = (props: CommentListProps) => {
  const { className, comments, error, isLoading } = props;

  return (
    <Stack
      gap={1}
      className={classNames(
        s.CommentList,
        { [s.loading]: isLoading },
        [className, s.max],
      )}
    >
      {isLoading ? (
        <Stack className={classNames(s.max, {}, [s.center])}>
          <Loader />
        </Stack>
      ) : (
        !error && (
          <>
            <Stack className={classNames(s.max)} gap={3}>
              {comments?.map(comment => (
                <CommentListItem comment={comment} key={comment.id} />
              ))}
              {(!comments || !comments.length) && <p>Комментарии отсутствуют</p>}
            </Stack>
          </>
        )
      )}
      {error && (
        <Stack direction="horizontal" className={classNames(s.max, {}, [s.center])}>
          Ошибка при загрузке комментариев.
        </Stack>
      )}
    </Stack>
  );
};