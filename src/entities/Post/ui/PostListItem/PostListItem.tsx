import { classNames } from 'shared/lib/classNames/classNames';
import { Card, Stack } from 'react-bootstrap';
import { Username } from 'shared/username';
import { Button } from 'react-bootstrap';
import { ReactNode, memo } from 'react';

interface PostListItemProps {
  email: string;
  title: string;
  body: string;
  userId: number;
  commentsNode?: ReactNode;
  commentsBtnOnClick?: () => void;
  commentsBtnText?: string;
  className?: string;
}

export const PostListItem = memo((props: PostListItemProps) => {
  const {
    className, email, body, title, userId, commentsNode, commentsBtnOnClick, commentsBtnText
  } = props;

  return (
    <Card className={classNames('', {}, [className])}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Username email={email} profileId={userId} avatar />
        <Card.Text>{body}</Card.Text>
        <Stack gap={3}>
          <Button variant='light' onClick={commentsBtnOnClick}>
            {commentsBtnText || 'Показать комментарии'}
          </Button>
          {commentsNode}
        </Stack>
      </Card.Body>
    </Card>
  );
});