import { classNames } from 'shared/lib/classNames/classNames';
import s from './PostList.module.scss';
import { Stack } from 'react-bootstrap';
import { Loader } from 'shared/ui/Loader';
import { ReactNode, memo } from 'react';

interface PostListProps {
  postsNode?: ReactNode;
  className?: string;
  isLoading?: boolean;
  error?: string;
}

export const PostList = memo((props: PostListProps) => {
  const { className, postsNode, error, isLoading } = props;

  return (
    <Stack
      gap={3}
      className={classNames(
        '',
        {},
        [className, s.max],
      )}
    >
      {isLoading ? (
        <Stack className={classNames(s.max, {}, [s.center])}>
          <Loader />
        </Stack>
      ) : (
        !error && (
          <>
            <Stack className={classNames(s.max)} gap={3}>
              {postsNode}
            </Stack>
          </>
        )
      )}
      {error && (
        <Stack direction="horizontal" className={classNames(s.max, {}, [s.center])}>
          Ошибка при загрузке постов.
        </Stack>
      )}
    </Stack>
  );
});