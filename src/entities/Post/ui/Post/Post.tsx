import { classNames } from 'shared/lib/classNames/classNames';

interface PostProps {
  className?: string;
}

export const Post = (props: PostProps) => {
  const { className } = props;

  return (
    <div className={classNames('', {}, [className])}>

    </div>
  );
};