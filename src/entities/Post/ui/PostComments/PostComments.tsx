import { classNames } from 'shared/lib/classNames/classNames';

interface PostCommentsProps {
  className?: string;
}

export const PostComments = (props: PostCommentsProps) => {
  const { className } = props;

  return (
    <div className={classNames('', {}, [className])}>

    </div>
  );
};