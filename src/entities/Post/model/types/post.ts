import { Comment } from "entities/Comment";
import { Profile } from "entities/Profile";

export interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
  profile?: Profile;

  comments?: Comment[];
  commentsIsLoading?: boolean;
  commentsError?: string;
}