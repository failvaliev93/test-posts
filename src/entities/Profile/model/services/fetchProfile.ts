import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/StoreProvider';
import { api } from 'shared/api/api';
import { Profile } from '../types/profile';

export const fetchProfile = createAsyncThunk<
  Profile,
  number,
  ThunkConfig<string>
>('user/fetchProfile', async (id, thunkAPI) => {
  try {
    const response = await api.get(`/users/${id}`);

    return response.data;

  } catch (error) {
    return thunkAPI.rejectWithValue('');
  }
});
