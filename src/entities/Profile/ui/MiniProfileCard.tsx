import { classNames } from 'shared/lib/classNames/classNames';
import { Avatar } from 'shared/ui/Avatar';
import { Loader } from 'shared/ui/Loader';
import { Profile } from '../model/types/profile';
import s from './Profile.module.scss';
import { Stack } from 'react-bootstrap';

interface ProfileProps {
  className?: string;
  profile?: Profile;
  isLoading?: boolean;
  error?: string;
}

export const MiniProfileCard = (props: ProfileProps) => {
  const {
    className,
    profile,
    error,
    isLoading,
  } = props;

  return (
    <Stack
      gap={3}
      className={classNames(
        s.ProfileCard,
        {},
        [className, s.max],
      )}
    >
      {isLoading ? (
        <Stack className={classNames(s.max, {}, [s.center])}>
          <Loader />
        </Stack>
      ) : (
        !error && (
          <>
            <Stack className={classNames(s.max, {}, [s.center])}>
              <Avatar alt='user avatar' />
            </Stack>
            <Stack className={classNames(s.max)}>
              <p>Id: {profile?.id}</p>
              <p>Имя: {profile?.name}</p>
              <p>Имя пользователя: {profile?.username}</p>
              <p>Почта: {profile?.email}</p>
            </Stack>
          </>
        )
      )}
      {error && (
        <Stack direction="horizontal" className={classNames(s.max, {}, [s.center])}>
          Ошибка при загрузке профиля.
        </Stack>
      )}
    </Stack>
  );
};
