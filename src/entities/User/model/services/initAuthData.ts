import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/StoreProvider';
import { User } from '../types/user';
import { api } from 'shared/api/api';

// авторизация
export const initAuthData = createAsyncThunk<
  User,
  void,
  ThunkConfig<string>
>('user/initAuthData', async (_, thunkAPI) => {
  try {
    const response = await api.get('/users/1');

    return response.data;

  } catch (error) {
    return thunkAPI.rejectWithValue('');
  }
});
