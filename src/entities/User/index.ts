export { initAuthData } from './model/services/initAuthData';
export type { User, UserSchema } from './model/types/user';
export { userActions } from './model/slice/userSlice';
export { getUserAuthData } from './model/selectors/getUserAuthData';