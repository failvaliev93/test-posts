export { UserPosts } from './ui/UserPosts/UserPosts';
export { userPostsActions } from './model/slice/userPostsSlice';
export type { UserPostsSchema } from './model/types/userPosts';