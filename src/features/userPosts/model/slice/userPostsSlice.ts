import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchPostsByUserId } from '../services/fetchPostsByUserId/fetchPostsByUserId';
import { UserPostsSchema } from '../types/userPosts';
import { Post } from 'entities/Post';
import { fetchUserPostComments } from '../services/fetchUserPostComments/fetchUserPostComments';

const initialState: UserPostsSchema = {
  isLoading: false,
  error: undefined,
  posts: undefined,
};

export const userPostsSlice = createSlice({
  name: 'userPosts',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPostsByUserId.pending, (state) => {
        state.error = undefined;
        state.isLoading = true;
      })
      .addCase(
        fetchPostsByUserId.fulfilled,
        (state, action: PayloadAction<Post[]>) => {
          state.isLoading = false;
          state.posts = action.payload;
        },
      )
      .addCase(fetchPostsByUserId.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      })
      .addCase(fetchUserPostComments.pending, (state, action) => {
        if (state.posts?.length) {
          state.posts.map((post) => {
            if (post.id === action.meta.arg) {
              post.commentsIsLoading = true;
              post.comments = undefined;
            }
            return post;
          })
        }
      })
      .addCase(
        fetchUserPostComments.fulfilled,
        (state, action) => {
          if (state.posts?.length) {
            state.posts.map((post) => {
              if (post.id === action.payload?.postId) {
                post.comments = action.payload.comments;
                post.commentsIsLoading = false;
              }
              return post;
            })
          }
        }
      )
      .addCase(fetchUserPostComments.rejected, (state, action) => {
        if (state.posts?.length) {
          state.posts.map((post) => {
            if (post.id === action.payload?.postId) {
              post.commentsError = 'error';
              post.commentsIsLoading = false;
            }
            return post;
          })
        }
      })
  },
});

export const { actions: userPostsActions } = userPostsSlice;
export const { reducer: userPostsReducer } = userPostsSlice;
