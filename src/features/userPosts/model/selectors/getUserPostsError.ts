import { StateSchema } from 'app/providers/StoreProvider';

export const getUserPostsError = (state: StateSchema) => state.userPosts?.error;
