import { StateSchema } from 'app/providers/StoreProvider';

export const getUserPostsData = (state: StateSchema) => state.userPosts?.posts;
