import { StateSchema } from 'app/providers/StoreProvider';

export const getUserPostsIsLoading = (state: StateSchema) => state.userPosts?.isLoading;
