import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/StoreProvider';
import { Post } from 'entities/Post/model/types/post';
import { delay } from 'shared/lib/delay/delay';

export const fetchPostsByUserId = createAsyncThunk<
  Post[],
  string,
  ThunkConfig<string>
>('profile/fetchPostsByUserId', async (userId, thunkApi) => {
  const { extra, rejectWithValue } = thunkApi;

  try {
    await delay();
    const response = await extra.api.get<Post[]>(`/posts?userId=${userId}`);

    if (!response.data) {
      throw new Error();
    }

    return response.data;
  } catch (e) {
    return rejectWithValue('error');
  }
});
