import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/StoreProvider';
import { Comment } from 'entities/Comment';
import { delay } from 'shared/lib/delay/delay';

export const fetchUserPostComments = createAsyncThunk<
  { comments: Comment[], postId: number },
  number,
  ThunkConfig<{ postId: number }>
>('profile/fetchUserPostComments', async (postId, thunkApi) => {
  const { extra, rejectWithValue } = thunkApi;

  try {
    await delay();
    const response = await extra.api.get<Comment[]>(`/posts/${postId}/comments`);

    if (!response.data) {
      throw new Error();
    }

    return { comments: response.data, postId };
  } catch (e) {
    return rejectWithValue({ postId });
  }
});