
import { Post } from 'entities/Post';

export interface UserPostsSchema {
  posts?: Post[];
  isLoading?: boolean;
  error?: string;
}
