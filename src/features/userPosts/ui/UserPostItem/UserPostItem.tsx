import { CommentList } from "entities/Comment";
import { Post, PostListItem } from "entities/Post";
import { useAppDispatch } from "shared/lib/hooks/useAppDispatch/useAppDispatch";
import { useCallback, useMemo, useState } from "react";
import { fetchUserPostComments } from "../../model/services/fetchUserPostComments/fetchUserPostComments";

interface UserPostItemProps {
  post: Post;
  email: string;
}

export const UserPostItem = (props: UserPostItemProps) => {
  const { post, email } = props;
  const dispatch = useAppDispatch();
  const [showComments, setshowComments] = useState(false);

  const commentsBtnOnClick = useCallback(() => {
    setshowComments(!showComments);
    if (!post.comments) {
      dispatch(fetchUserPostComments(post.id));
    }
  }, [dispatch, post.comments, post.id, showComments]);

  const commentsNode =
    (
      <CommentList
        comments={post.comments}
        error={post.commentsError}
        isLoading={post.commentsIsLoading}
      />
    );

  const commentsBtnText = useMemo(() => {
    return !showComments ? 'Показать комментарии' : 'Скрыть комментарии'
  }, [showComments]);

  return (
    <PostListItem
      key={post.id}
      body={post.body}
      title={post.title}
      userId={post.userId}
      email={email}
      commentsNode={showComments && commentsNode}
      commentsBtnOnClick={commentsBtnOnClick}
      commentsBtnText={commentsBtnText}
    />
  );
};