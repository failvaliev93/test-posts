import { useSelector } from 'react-redux';
import { getUserPostsData } from '../../model/selectors/getUserPostsData';
import { getUserPostsError } from '../../model/selectors/getUserPostsError';
import { getUserPostsIsLoading } from '../../model/selectors/getUserPostsIsLoading';
import { PostList } from 'entities/Post';
import { useInitEffect } from 'shared/lib/hooks/useInitEffect/useInitEffect';
import { useAppDispatch } from 'shared/lib/hooks/useAppDispatch/useAppDispatch';
import { fetchPostsByUserId } from '../../model/services/fetchPostsByUserId/fetchPostsByUserId';
import { UserPostItem } from '../UserPostItem/UserPostItem';
import { getProfileData } from 'features/profileCard';

interface UserPostsProps {
  userId?: string;
  className?: string;
}

export const UserPosts = (props: UserPostsProps) => {
  const { className, userId } = props;
  const dispatch = useAppDispatch();
  const posts = useSelector(getUserPostsData);
  const error = useSelector(getUserPostsError);
  const isLoading = useSelector(getUserPostsIsLoading);
  const profileData = useSelector(getProfileData);

  useInitEffect(() => {
    if (userId) {
      dispatch(fetchPostsByUserId(userId));
    }
  });

  const postsNode = posts && posts.length > 0
    ?
    posts.map((post) => (
      <UserPostItem
        key={post.id}
        post={post}
        email={profileData?.email || ''}
      />
    ))
    : <p>Посты отсутствуют</p>;

  return (
    <>
      <h4>Посты пользователя</h4>
      <PostList
        isLoading={isLoading}
        postsNode={postsNode}
        error={error}
        className={className}
      />
    </>
  );
};