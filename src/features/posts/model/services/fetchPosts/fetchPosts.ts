import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/StoreProvider';
import { Post } from 'entities/Post/model/types/post';
import { fetchProfile } from 'entities/Profile/model/services/fetchProfile';
import { delay } from 'shared/lib/delay/delay';

export const fetchPosts = createAsyncThunk<
  Post[],
  number,
  ThunkConfig<string>
>('profile/fetchPosts', async (limit, thunkApi) => {
  const { extra, rejectWithValue } = thunkApi;

  try {
    await delay();
    const response = await extra.api.get<Post[]>(`/posts?_limit=${limit}`);
    let result = response.data;

    if (!result) {
      throw new Error();
    }

    const usedIdArr: number[] = [];
    result.forEach(async (post, i) => {
      if (!usedIdArr.includes(post.userId)) {
        thunkApi.dispatch(fetchProfile(post.userId));
        usedIdArr.push(post.userId);
      }
    })

    return result;
  } catch (e) {
    console.log(e);
    return rejectWithValue('error');
  }
});
