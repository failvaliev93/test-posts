
import { Post } from 'entities/Post';

export interface PostsSchema {
  posts?: Post[];
  isLoading?: boolean;
  error?: string;
}
