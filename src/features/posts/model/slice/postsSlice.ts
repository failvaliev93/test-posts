import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchPosts } from '../services/fetchPosts/fetchPosts';
import { PostsSchema } from '../types/posts';
import { Post } from 'entities/Post';
import { fetchProfile } from 'entities/Profile/model/services/fetchProfile';
import { fetchPostComments } from '../services/fetchPostComments/fetchPostComments';
import { Comment } from 'entities/Comment';

const initialState: PostsSchema = {
  isLoading: false,
  error: undefined,
  posts: undefined,
};

export const postsSlice = createSlice({
  name: 'Posts',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPosts.pending, (state) => {
        state.error = undefined;
        state.isLoading = true;
      })
      .addCase(
        fetchPosts.fulfilled,
        (state, action: PayloadAction<Post[]>) => {
          state.isLoading = false;
          state.posts = action.payload;
        },
      )
      .addCase(fetchPosts.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      })
      .addCase(fetchProfile.fulfilled, (state, action) => {
        if (state.posts?.length) {
          state.posts.map((post) => post.userId === action.payload.id ? post.profile = action.payload : post)
        }
      })
      .addCase(fetchPostComments.pending, (state, action) => {
        if (state.posts?.length) {
          state.posts.map((post) => {
            if (post.id === action.meta.arg) {
              post.commentsIsLoading = true;
              post.comments = undefined;
            }
            return post;
          })
        }
      })
      .addCase(
        fetchPostComments.fulfilled,
        (state, action: PayloadAction<{ comments: Comment[], postId: number }>) => {
          if (state.posts?.length) {
            state.posts.map((post) => {
              if (post.id === action.payload?.postId) {
                post.comments = action.payload.comments;
                post.commentsIsLoading = false;
              }
              return post;
            })
          }
        },
      )
      .addCase(fetchPostComments.rejected, (state, action) => {
        if (state.posts?.length) {
          state.posts.map((post) => {
            if (post.id === action.payload?.postId) {
              post.commentsError = 'error';
              post.commentsIsLoading = false;
            }
            return post;
          })
        }
      })
  },
});

export const { actions: postsActions } = postsSlice;
export const { reducer: postsReducer } = postsSlice;
