export { Posts } from './ui/Posts/Posts';
export { postsActions } from './model/slice/postsSlice';
export type { PostsSchema } from './model/types/posts';