import { useSelector } from 'react-redux';
import { PostList } from 'entities/Post';
import { useInitEffect } from 'shared/lib/hooks/useInitEffect/useInitEffect';
import { useAppDispatch } from 'shared/lib/hooks/useAppDispatch/useAppDispatch';
import { fetchPosts } from '../../model/services/fetchPosts/fetchPosts';
import { getPostsData } from '../../model/selectors/getPostsData';
import { getPostsError } from '../../model/selectors/getPostsError';
import { getPostsIsLoading } from '../../model/selectors/getPostsIsLoading';
import { PostItem } from '../PostItem/PostItem';

interface UserPostsProps {
  className?: string;
}

export const Posts = (props: UserPostsProps) => {
  const { className } = props;
  const dispatch = useAppDispatch();
  const posts = useSelector(getPostsData);
  const error = useSelector(getPostsError);
  const isLoading = useSelector(getPostsIsLoading);

  useInitEffect(() => {
    dispatch(fetchPosts(20));
  });

  const postsNode = posts && posts.length > 0
    ?
    posts.map((post) => (
      <PostItem
        key={post.id}
        post={post}
      />
    ))
    : <p>Посты отсутствуют</p>;

  return (
    <>
      <h4>Посты</h4>
      <PostList
        isLoading={isLoading}
        postsNode={postsNode}
        error={error}
        className={className}
      />
    </>
  );
};