import { CommentList } from "entities/Comment";
import { Post, PostListItem } from "entities/Post";
import { fetchPostComments } from "../../model/services/fetchPostComments/fetchPostComments";
import { useCallback, useMemo, useState } from "react";
import { useAppDispatch } from "shared/lib/hooks/useAppDispatch/useAppDispatch";

interface PostItemProps {
  post: Post;
}

export const PostItem = (props: PostItemProps) => {
  const { post } = props;
  const dispatch = useAppDispatch();
  const [showComments, setshowComments] = useState(false);

  const commentsBtnOnClick = useCallback(() => {
    setshowComments(!showComments);
    if (!post.comments) {
      dispatch(fetchPostComments(post.id));
    }
  }, [dispatch, post.comments, post.id, showComments]);

  const commentsNode =
    (
      <CommentList
        comments={post.comments}
        error={post.commentsError}
        isLoading={post.commentsIsLoading}
      />
    );

  const commentsBtnText = useMemo(() => {
    return !showComments ? 'Показать комментарии' : 'Скрыть комментарии'
  }, [showComments]);

  return (
    <PostListItem
      key={post.id}
      body={post.body}
      title={post.title}
      userId={post.userId}
      email={post.profile?.email || ''}
      commentsNode={showComments && commentsNode}
      commentsBtnOnClick={commentsBtnOnClick}
      commentsBtnText={commentsBtnText}
    />
  );
};