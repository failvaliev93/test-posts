import { memo } from 'react';
import { useSelector } from 'react-redux';
import { useAppDispatch } from 'shared/lib/hooks/useAppDispatch/useAppDispatch';
import { MiniProfileCard } from 'entities/Profile';
import { getProfileError } from '../../model/selectors/getProfileError/getProfileError';
import { getProfileIsLoading } from '../../model/selectors/getProfileIsLoading/getProfileIsLoading';
import { fetchProfileData } from '../../model/services/fetchProfileData/fetchProfileData';
import { useInitEffect } from 'shared/lib/hooks/useInitEffect/useInitEffect';
import { getProfileData } from 'features/profileCard/model/selectors/getProfileData/getProfileData';
import { Stack } from 'react-bootstrap';

interface ProfileCardProps {
  id?: string;
  className?: string;
}

export const ProfileCard = memo((props: ProfileCardProps) => {
  const { className, id: profileId } = props;
  const dispatch = useAppDispatch();
  const profile = useSelector(getProfileData);
  const error = useSelector(getProfileError);
  const isLoading = useSelector(getProfileIsLoading);

  useInitEffect(() => {
    if (profileId) {
      dispatch(fetchProfileData(profileId));
    }
  });

  return (
    <Stack>
      <h4>Пользователь</h4>
      <MiniProfileCard
        profile={profile}
        isLoading={isLoading}
        error={error}
        className={className}
      />
    </Stack>
  );
});
