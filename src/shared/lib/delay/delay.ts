
export const delay = (time: number = 1500) => {
  return new Promise(resolve => setTimeout(resolve, time));
};