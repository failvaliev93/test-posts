// import { classNames } from './classNames';
import { classNames } from './classNames';

describe('classNames', () => {
  test('с одним параметром', () => {
    expect(classNames('someclass')).toBe('someclass');
  });

  test('с двумя параметрами', () => {
    expect(classNames('someclass', {}, ['cls1', 'cls2'])).toBe(
      'someclass cls1 cls2',
    );
  });

  test('с тремя параметрами', () => {
    expect(classNames('someclass', { cls3: true }, ['cls1', 'cls2'])).toBe(
      'someclass cls1 cls2 cls3',
    );
  });

  test('с тремя параметрами, но cls3: false', () => {
    expect(classNames('someclass', { cls3: false }, ['cls1', 'cls2'])).toBe(
      'someclass cls1 cls2',
    );
  });

  test('с тремя параметрами, но cls3: undefined', () => {
    expect(classNames('someclass', { cls3: undefined }, ['cls1', 'cls2'])).toBe(
      'someclass cls1 cls2',
    );
  });
});
