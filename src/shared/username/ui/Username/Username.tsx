import { classNames } from 'shared/lib/classNames/classNames';
import { Stack } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getRouteProfile } from 'shared/const/router';
import { Avatar } from 'shared/ui/Avatar';

interface UsernameProps {
  profileId: number;
  email: string;
  className?: string;
  avatar?: boolean;
  onClick?: () => void;
}

export const Username = (props: UsernameProps) => {
  const { className, profileId, avatar, email, onClick } = props;

  return (
    <Link to={getRouteProfile(`${profileId}`)} className={classNames('', {}, [className])}>
      <Stack direction="horizontal" gap={3} onClick={onClick}>
        {avatar && <Avatar />}
        {email}
      </Stack>
    </Link>
  );
};