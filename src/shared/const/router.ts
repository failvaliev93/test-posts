export enum AppRoutes {
  MAIN = 'main',
  PROFILE = 'profile',
  ABOUT = 'about',
  NOT_FOUND = 'not_found', // last
}

export const getRouteMain = () => '/';
export const getRouteProfile = (id: string) => `/profile/${id}`;
export const getRouteAbout = () => '/about';
