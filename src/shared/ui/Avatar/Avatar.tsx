import { useMemo, CSSProperties } from 'react';
import { classNames } from 'shared/lib/classNames/classNames';
import { AppImage } from '../AppImage';
import UserIcon from '../../assets/icons/user.png';
import s from './Avatar.module.scss';

interface AvatarProps {
  className?: string;
  src?: string;
  alt?: string;
  size?: number;
}

export const Avatar = (props: AvatarProps) => {
  const { className, src = UserIcon, alt = 'Avatar', size = 50 } = props;

  const styles = useMemo<CSSProperties>(
    () => ({
      width: size,
      height: size,
    }),
    [size],
  );

  return (
    <AppImage
      className={classNames(s.Avatar, {}, [className])}
      style={styles}
      src={src}
      alt={alt}
    />
  );
};
