import { getRouteAbout, getRouteMain } from "shared/const/router";
import { INavbarItem } from "../types/navbar";
import { createSelector } from '@reduxjs/toolkit';

export const getNavbarItems = createSelector(() => {
  const navbarItemsList: INavbarItem[] = [
    {
      path: getRouteMain(),
      text: 'Список постов',
    },
    {
      path: getRouteAbout(),
      text: 'Обо мне',
    }
  ]
  return navbarItemsList;
});