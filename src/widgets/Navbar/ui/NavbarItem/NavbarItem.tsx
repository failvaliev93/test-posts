import { classNames } from 'shared/lib/classNames/classNames';
import { Nav } from 'react-bootstrap';
import { INavbarItem } from 'widgets/Navbar/model/types/navbar';
import { NavLink } from 'react-router-dom';

interface NavbarItemProps {
  item: INavbarItem;
  className?: string;
  onClick?: () => void;
}

export const NavbarItem = (props: NavbarItemProps) => {
  const { className, item, onClick } = props;

  return (
    <Nav.Link
      className={classNames('', {}, [className])}
      as={NavLink}
      to={item.path}
      onClick={onClick}
    >
      {item.text}
    </Nav.Link>
  );
};