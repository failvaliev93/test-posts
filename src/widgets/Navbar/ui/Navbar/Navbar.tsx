import { classNames } from 'shared/lib/classNames/classNames';
import { memo, useMemo, useState } from 'react';
import NavbarBt from 'react-bootstrap/Navbar';
import { Container, Nav, Offcanvas } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { getNavbarItems } from '../../model/selectors/getNavbarItems';
import { NavbarItem } from '../NavbarItem/NavbarItem';
import { getUserAuthData } from 'entities/User';
import { Username } from 'shared/username';

interface NavbarProps {
  className?: string;
}

export const Navbar = memo((props: NavbarProps) => {
  const { className } = props;
  const expand = false;
  const sidebarItemsList = useSelector(getNavbarItems);
  const userAuthData = useSelector(getUserAuthData);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const itemsList = useMemo(() => {
    return sidebarItemsList.map((item) => (
      <NavbarItem item={item} key={item.path} onClick={handleClose} />
    ));
  }, [sidebarItemsList]);

  return (
    <div className={classNames('', {}, [className])}>
      <NavbarBt bg="light" expand={expand} className="mb-3">
        <Container fluid>
          <NavbarBt.Toggle
            aria-controls={`offcanvasNavbar-expand-${expand}`}
            onClick={handleShow}
          />
          <NavbarBt.Brand href="#">Test</NavbarBt.Brand>
          <NavbarBt.Offcanvas
            id={`offcanvasNavbar-expand-${expand}`}
            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
            placement="start"
            onHide={handleClose}
            show={show}
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                Меню
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              {userAuthData &&
                (
                  <Username
                    avatar
                    profileId={userAuthData.id}
                    email={userAuthData.email}
                    onClick={handleClose}
                  />
                )}
              <Nav className="justify-content-end flex-grow-1 pe-3">
                {itemsList}
              </Nav>
            </Offcanvas.Body>
          </NavbarBt.Offcanvas>
        </Container>
      </NavbarBt>
    </div >
  );
});