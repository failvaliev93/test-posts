import { Page } from 'widgets/Page';

interface AboutPageProps {
}

export const AboutPage = (props: AboutPageProps) => {

  return (
    <Page >
      <a target='_blank' href="https://kazan.hh.ru/resume/f4e8d651ff01ddf8c40039ed1f6f364e70586a" rel="noreferrer">Резюме</a>
    </Page>
  );
};