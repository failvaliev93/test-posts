import { Page } from 'widgets/Page';
import { Posts } from 'features/posts';

interface MainPageProps {
}

export const MainPage = (props: MainPageProps) => {

  return (
    <Page>
      <Posts />
    </Page>
  );
};