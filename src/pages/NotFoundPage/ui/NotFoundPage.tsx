import { classNames } from 'shared/lib/classNames/classNames';
import { Page } from 'widgets/Page';
import s from './NotFoundPage.module.scss';

interface NotFoundPageProps {
  className?: string;
}

export const NotFoundPage = ({ className }: NotFoundPageProps) => {
  return (
    <Page
      data-testid='NotFoundPage'
      className={classNames(s.NotFoundPage, {}, [className])}
    >
      Страница не найдена
    </Page>
  );
};
