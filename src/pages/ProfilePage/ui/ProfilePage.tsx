import { classNames } from 'shared/lib/classNames/classNames';
import { Page } from 'widgets/Page';
import { useNavigate, useParams } from 'react-router-dom';
import { ProfileCard } from 'features/profileCard';
import { Button, Stack } from 'react-bootstrap';
import { useCallback } from 'react';
import { UserPosts } from 'features/userPosts';
import { getRouteMain } from 'shared/const/router';

export interface ProfilePageProps {
  className?: string;
}

export const ProfilePage = ({ className }: ProfilePageProps) => {
  const { id } = useParams<{ id: string }>();
  const navigate = useNavigate();

  const onBack = useCallback(() => {
    navigate(getRouteMain());
  }, [navigate]);

  return (
    <Page
      data-testid='ProfilePage'
      className={classNames('s.ProfilePage', {}, [className])}
    >
      <Stack gap={3}>
        <Button variant='light' onClick={onBack}>
          Назад
        </Button>
        <ProfileCard id={id} />
        <UserPosts userId={id} />
      </Stack>
    </Page>
  );
};
